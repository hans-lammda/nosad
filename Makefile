.PHONY: content server

all: content generate server test 

content: 
	make -C content build

generate: 
	make -C content generate

server: 
	make -C server build

test: 
	make -C server server

