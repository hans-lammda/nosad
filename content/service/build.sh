#!/bin/bash 

OUT="build"
SRC="src"
PROJECT="18440910"
STAGE=$OUT/"staging"
BUILD_INFO=$OUT/build.txt

# tear down and re-build the output directory
rm -rf $OUT
mkdir $OUT
mkdir $STAGE
mkdir $OUT/time
mkdir $OUT/style

cp $SRC/assets/stilmallar/custom/custom-min.css $OUT/style/custom-min.css
cp $SRC/assets/stilmallar/custom/style.css $OUT/style/custom-min.css

if [ $# -ne 0 ]
  then
# Build css, use only local
    sass src/assets/stilmallar/bootstrap-scss/custom.scss $OUT/style/custom.css
  #  purgecss --css $OUT/style/custom.css --content $SRC/assets/template/css-template.html --output $OUT/style
    minify $OUT/style/custom.css > $OUT/style/custom-min.css
fi

#sass src/assets/_scss/styles.scss $OUT/styles.css

cat $OUT/style/custom-min.css | jq -Rs '{"style":.}' > $OUT/style/style.json
curl -s https://gitlab.com/api/v4/projects/$PROJECT/wikis?with_content=true | jq -cr '.[] | .slug, .' | awk 'NR%2{f=var$0;next} {print >f;close(f)}' var="${STAGE}/"
#curl -s https://gitlab.com/api/v4/projects/21882815/repository/commits | jq '.[0].created_at' | xargs -I{} gdate -d {} '+%Y-%m-%d %H:%M' | jq -Rs '{"updated":("Uppdaterad "+.)}' > $OUT/time/update_time.json

#CO2
curl -s https://api.websitecarbon.com/b?url=https%3A%2F%2Fnosad.se%2F > $OUT/CO2.json

FILES="$STAGE/*"
for f in $FILES
do
  echo "Processing $f"
  # take action on each file. $f store current file name
  cat "$f" | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' > "$f"-pre
  jq -s '.[0] * .[1] * .[2]' "$f"-pre $OUT/style/style.json $OUT/CO2.json | mustache - $SRC/assets/template/index.mustache > "$f".html
done

jq -s '.[0] * .[1]' $STAGE/Programvaror-pre $OUT/style/style.json | mustache - $SRC/assets/template/offentligkod.mustache > $OUT/offentligkod.html

#cp $SRC/assets/template/index-grid.html $OUT/home2.html
#cp $SRC/assets/template/test-grid.html $OUT/home3.html
mv $STAGE/*html $OUT
cp $SRC/assets/template/test-kunskapsbank.html $OUT/test-kunskapsbank.html
cp -R $SRC/static/site/* $OUT
cp $SRC/static/img/workshop.jpg $OUT/
cp $OUT/home.html $SRC/assets/template/css-template.html
#cp $OUT/style/custom-min.css $SRC/assets/stilmallar/custom/custom-min.css
gzip -k $OUT/*html

rm -rd $STAGE
#rm $OUT/style/custom.css
rm $OUT/style/style.json

echo "Build $(date +"%Y-%m-%dT%H:%M:%SZ")" > $BUILD_INFO
