#!/bin/sh

if [ "$1" = 'generate' ]; then
    (cd /build && bash build.sh 2> /dev/null )
    ( cd /build && cp -r build /static ) 
else
    exec "$@"
fi

