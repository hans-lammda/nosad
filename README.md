# Nosad & BomResolver



The Nosad wiki is hosted by a Alpine container, BomResolver starts with a list of all packages and delivers metadata required for rebuild in isolation. 





The **build.sh** scripts connects to Gitlab Wiki API and extracts all information, then it generates static content that could be served by any web server.  in addition to jq also two npm packages are required marked and mustache. 

The original multistage container have been replaced with two Alpine containers. 

- content_builder
- http_service

### content builder

This container connects to gitlabs wiki api and writes static content to directory  ./static 



### http_server

This container contains lighttpd and all content from ./static. The service is adopted for Kubernetes with a route "nosad". 

#### Scalability 

nosad/server/deploy/chart/nosad/values.yaml

`replicaCount: 3`

```
NAME                     READY   STATUS    RESTARTS   AGE
nosad-6c4f776cb5-5fkrw   1/1     Running   0          15s
nosad-6c4f776cb5-mnwlp   1/1     Running   0          15s
nosad-6c4f776cb5-rz4qp   1/1     Running   0          5m52s

```



# Software Bill of material 

There are several emerging standards for SBOMS, such as CycloneDX and SPDX. 

The Bomresolver is designed for complete rebuild in isolation and contains more information such as the tools required for rebuild. 

The resolved.json could be post processed to generate dependency tree and project health graphs.  In the reports directory there is two examples of Alpine versions that indications how different projects are curated. 

 

├── resolver
│   ├── dependencies_3.18.svg
│   ├── health_3.17.svg
│   ├── health_3.18.svg
│   ├── packages
│   └── resolved.json
└── sbom
    ├── cyclonedx.json
    └── spdx.json



# Build

```
all: content generate server test 

content: 
	make -C content build

generate: 
	make -C content generate

server: 
	make -C server build

test: 
	make -C server server

```

# 



## Authors and acknowledgment
hans@lammda.se 

## License
MIT License 

